# BACK API "RIPLEY"

## Intro 
Este proyecto fue creado para una prueba para el banco ripley

## Directories
```
├── config/     
│   ├── config.js  
│     
├── connection/      
│   ├── connection.js  
│     
├── controllers/     
│   ├── clienntes.js  
│   ├── orden_compra.js    
│   ├── productos.js    
│   └── venta.js 
│    
├── public/      (static files)
│   ├── favicon.ico  
│   └── index.html (html temlpate)
│
├── routes/           
│  └── web.js    (routes config)
│
├── sql.sql 
└── package.json
```

#### Para iniciar el proyecto 
- Debemos tener iniciado el XAMP 
- ingresar a la carpeta `back_proyecto_ripley` 
- Instalar dependencias con `npm i` o `npm install`

## BASE DE DATOS
- correr el arhivo sql que se ecuentra en back_proyecto_ripley

## Sctipts para inciar
`npm start`   
`npm run dev`
`npm run build`    

