'use strict'

const app = require('./app');
const config = require('./config/config');

var server = app.listen(config.PORT, () => {
    console.log(`base de datos corriendo en http://localhost:${config.PORT}`);
});

server.timeout = 600000;

