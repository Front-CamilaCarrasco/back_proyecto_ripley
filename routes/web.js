'use strict'

const express = require('express');
const api = express.Router();
const productCtrl = require('../controllers/productos');
const clientesCtrl = require('../controllers/cliente');
const ventasCtrl = require('../controllers/venta');
const ordenCompraCtrl = require('../controllers/orden_compra');
const db = require('../connection/connection');

api.get('/', (req, res) => res.sendFile('/public/index.html', { root: '.' }));


api.get('/getProductos', productCtrl.obtenerProductos);
api.get('/getOrdenCompra/:oc_codigo', ordenCompraCtrl.obtenerOrdenCompra);
api.post('/nuevaOrdenCompra', ordenCompraCtrl.nuevaOrdenCompra);
api.get('/getClientes', clientesCtrl.obtenerClientes);
api.post('/nuevoCliente', clientesCtrl.nuevoCliente);
api.post('/nuevaVenta', ventasCtrl.nuevaVenta);



module.exports = api;




