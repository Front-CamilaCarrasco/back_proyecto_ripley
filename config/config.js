module.exports = {
    PORT: process.env.PORT || 8000,
    PORT_IO: process.env.PORT_IO || 8001,
    TOKEN_EXPIRED: process.env.TOKEN_EXPIRED = 60 * 60 * 24,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'ripley_bd'
};

