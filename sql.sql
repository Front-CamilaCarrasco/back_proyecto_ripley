
-- Volcando estructura de base de datos ripley_bd
DROP DATABASE IF EXISTS `curso`;
CREATE DATABASE IF NOT EXISTS `ripley_bd` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ripley_bd`;



-- Volcando estructura para tabla Productos
CREATE TABLE `Productos` (
  `Prod_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de Producto',
  `Prod_marca` VARCHAR(50)  NOT NULL DEFAULT '' COMMENT 'Marca de Producto',
  `Prod_codigo` VARCHAR(50)  NOT NULL DEFAULT '' COMMENT 'codigo de Producto',
  `Prod_nombre` varchar(80) NOT NULL DEFAULT '' COMMENT 'Nombre del Producto',
  `Prod_descripcion` varchar(150) NOT NULL DEFAULT '' COMMENT 'Descripcion del Producto',
  `Prod_img` VARCHAR(150) NOT NULL DEFAULT '' COMMENT 'imagen del Producto',
  `Prod_Precio` int(150) NOT NULL DEFAULT 0 COMMENT 'Precio del Producto',
  `Prod_Stock` int(50) NOT NULL DEFAULT 0 COMMENT 'Stock del Producto',
  `Prod_Filtro` varchar(50) NOT NULL DEFAULT 0 COMMENT 'filtro del Producto',
  PRIMARY KEY (`Prod_id`)
);

CREATE TABLE `clientes` (
  `cli_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de cliente',
  `cli_rut` varchar(13) NOT NULL DEFAULT 0 COMMENT 'rut de cliente',
  `cli_telefono` varchar(13) NOT NULL DEFAULT 0 COMMENT 'telefono de cliente',
  `cli_nombre` varchar(70) NOT NULL DEFAULT 0 COMMENT 'nombres de cliente',
  `cli_apellido` varchar(70) NOT NULL DEFAULT 0 COMMENT 'apellidos de cliente',
  `cli_email` varchar(70) NOT NULL DEFAULT 0 COMMENT 'email de cliente',
	`cli_direccion` varchar(70) NOT NULL DEFAULT 0 COMMENT 'direccion de cliente',
  `cli_nro_tarjeta` varchar(70) NOT NULL DEFAULT 0 COMMENT 'nro tarjeta de cliente',
  PRIMARY KEY (`cli_id`)
  );


CREATE TABLE `ventas` (
  `v_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de carrito',
  `v_prod_id` int(10) UNSIGNED  NOT NULL DEFAULT 0 COMMENT 'id producto venta',
  `v_cli_id` int(10) UNSIGNED    NOT NULL DEFAULT 0 COMMENT 'id cliente venta',
  PRIMARY KEY (`v_id`),
  CONSTRAINT fk_ventas_producto FOREIGN KEY (v_prod_id) REFERENCES productos(Prod_id),
  CONSTRAINT fk_ventas_cliente FOREIGN KEY (v_cli_id) REFERENCES clientes(cli_id)
);



CREATE TABLE `orden_compra` (
  `od_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de carrito',
  `od_v_id`  int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'id venta',
  `oc_codigo` int(10) UNSIGNED  NOT NULL DEFAULT 0 COMMENT 'codigo orden compra',
  `oc_fecha_emision` TIMESTAMP   NOT NULL DEFAULT 0 COMMENT ' fecha orden compra',
  PRIMARY KEY (`od_id`),
  CONSTRAINT fk_orden_venta FOREIGN KEY (od_v_id) REFERENCES ventas(v_id)
);




INSERT INTO `productos` (`Prod_Id`,`prod_codigo`,`prod_marca`,`Prod_nombre`, `Prod_descripcion`, `Prod_img`,`Prod_Precio`,`prod_stock`, `Prod_filtro`)
VALUES
(1,
	2000327421128 ,
	'CAROLINA HERRERA',
	'CAROLINA HERRERA 212 SEXY 60 ML EDICION LIMITADA',
	'Una materialización de la gracia y el estilo, la 212 SEXY de mujer es extremadamente femenina y seductora, y su perfume es el complemento esencial para reflejar la sensualidad y la sofisticación que la caracterizan. Es tan suave, delicada y misteriosa como el crepúsculo de su ciudad. La nota superior mantiene la frescura de la 212 original, aunque enriquecida con un toque sutil y picante de pimienta rosa. Un perfume dulce y sexy para mujeres, la reproducción del aroma del algodón de azúcar le aporta un toque ultra femenino a la fragancia.',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000327421128/2000327421128_2.jpg',
	 29990,
	 100,
	'perfumeria'),
(2,
	2000357756245 ,
	'ARMANI',
	'GIORGIO ARMANI ACQUA DI GIO 200 ML EDICION LIMITADA',
	'El Perfume Acqua Di Gio EDT 200 ml Armani es uno de los aromas favoritos de los hombres modernos, actuales y espontáneos, porque logra mezclar de manera majestuosa las notas dulces y saladas, generando un aroma cítrico, con toques agridulces, que evoca la piel repleta de agua y de sol, manteniéndolos frescos y atractivos todo el día.',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000357756245/2000357756245-1.jpg',
	 64990,
	 340,
	'perfumeria'),
	(3,
	2000337591163,
	'LANCÔME',
	'LANCOME TRÉSOR MIDNIGHT ROSE 30 ML EDICION LIMITADA',
	'Eau de Parfum Conquista corazones con la fragancia traviesa y chispeante de Lancôme, Trésor Midnight Rose. Juega a los juegos del amor con este cautivador aroma que mezcla notas de rosa intensa y grosella negra con irresistibles notas de jazmín, peonía y pimienta rosada creando un buqué floral lleno de magia. Encantadora y sensual, sorprende con Trésor Midnight Rose.',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000337591163/2000337591163_2.jpg',
	 26990,
	 170,
	'perfumeria'),
(4,
	2000363099206,
	'RALPH LAUREN',
	'PERFUME RALPH LAUREN POLO SPORT 125 ML EDICION LIMITADA',
	'El perfume fitness. Polo Sport de Ralph Lauren representa la vela, el esqui, el senderismo, el rafting, el trekking, el fútbol y el aire libre… Además, Polo Sport es más que un deporte, es una actitud, un estilo de vida, una mezcla de bienestar y salud espiritual.',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000363099206/2000363099206_2.jpg',
	 39990,
	 500,
	'perfumeria'),
(5,
	2000374340946 ,
	'PACO RABANNE',
	'PACO RABANNE INVICTUS EDT 150 ML',
	'Invictus, es el aroma de la victoria, la esencia de los héroes, una fragancia de firma amaderada y fresca que confronta dos fuerzas inesperadas: el éxtasis y la adicción. Escalofrío de éxtasis: una sensación de energía pura, un frescor vibrante como una subida de adrenalina. Adicción sensual: la atracción de un magnetismo sensual que simboliza un poder viril y masculino. ¡Invictus, cuando lo usas puedes ganar!',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000374340946/2000374340946_2.jpg',
	 54990,
	 400,
	'perfumeria'),
(6,
	2000376375304 ,
	'ARMANI',
	'ACQUA DI GIO PROFUMO EDP 180 ML GIORGIO ARMANI',
	'Acqua di Gio Profumo es la nueva intensidad mineralizada. Es el reflejo de la naturaleza pura, cuando la luminosidad del mar se junta con el poder de la roca. Es el Acqua negra de Giorgio Armani. Esta fragancia trae un nuevo concepto de masculinidad, que trata de un tipo diferente de sensualidad, algo más salvaje',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000376375304/2000376375304_2.jpg',
	 69990,
	 600,
	'perfumeria'),
(7,
	200000586205 ,
	'BRITNEY SPEARS',
	'MIDNIGHT EDP 100ML',
	'Midnight Fantasy de Britney Spears es una fragancia de la familia olfativa Floral Frutal para Mujeres. Midnight Fantasy se lanzó en 2006. La Nariz detrás de esta fragrancia es Caroline Sabas. Las Notas de Salida son ciruela y cereza ácida (guinda); las Notas de Corazón son iris , orquídea y fresia; las Notas de Fondo son ámbar, almizcle y vainilla.',
	'https://ripleycl.imgix.net/https%3A%2F%2Fi.ibb.co%2Fk2LyZrW%2FBRSP1.jpg?w=750&h=555&ch=Width&auto=format&cs=strip&bg=FFFFFF&q=60&trimcolor=FFFFFF&trim=color&fit=fillmax&ixlib=js-1.1.0&s=6db633f98c29f5672ab95278ae174166',
	 19990,
	 100,
	'perfumeria'),
(8,
	2000378487722 ,
	'YVES SAINT LAURENT',
	'Y EDT 200 ML YVES SAINT LAURENT',
	' Inspirado en la icónica camiseta blanca Yves Saint Laurent y la chaqueta negra, este Eau De Toilette representa un equilibrio entre frescura y fuerza. Las notas de bergamota, salvia y jengibre ofrecen una frescura jugosa y aguda para desafiar la convención y superar todas las expectativas. En su base, las suaves notas de abeto balsámico, madera de cedro y ámbar gris marino encarnan el sutil poder de una chaqueta negra a medida que es refinada, elegante y sorprendentemente poderosa. Una creación auténtica y audaz de una masculinidad redefinida.',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000378487722/2000378487722_2.jpg',
	 64990,
	 100,
	'perfumeria'),
(9,
	2000327169006 ,
	'ANTONIO BANDERAS',
	'PERFUME ANTONIO BANDERAS MEDITERRANEO 200 ML EDICION LIMITADA',
	'El perfume Mediterráneo de Antonio Banderas es el segundo que crea el actor español. Su aroma es fresco y varonil, evocando las sensaciones que producen la playa, las olas y el viento en esas costas. Notas cítricas y aromáticas con un fondo profundo y frescor vetiver-ámbar.',
	'https://home.ripley.cl/store/Attachment/WOP/D328/2000327169006/2000327169006-4.jpg',
	 13990,
	 100,
	'perfumeria'),
(10,
	2000327421128,
	'HALLOWEEN',
	'PERFUME HALLOWEEN EDT 100 ML EDL',
	'Halloween es para las mujeres misteriosas que combinan la seducción y la inocencia, y contrastan en sí mismas la claridad y la oscuridad. Es una esencia sexy que penetra en la piel y envuelve a la mujer en su misterio, su aroma irresistible cautiva a cualquiera y es el reflejo de un carácter dual: la mujer romántica que conquista con angelical embrujo. Atrévete a dominar el mundo a tu paso con Halloween y deja salir tu lado sexy y romántico.',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000327421128/2000327421128_2.jpg',
	 29990,
	 100,
	'perfumeria'),
(11,
	2000373259768,
	'PACO RABANNE',
	'PACO RABANNE ONE MILLION EDT 50 ML',
	'El hombre atrevido que juega a la seducción extrema… su nueva arma se llama 1 Million, un amaderado especiado, un auténtico concentrado de fantasías. Nada es lo suficientemente bello para este seductor, cuyo frasco, evoca la irresistible forma de un lingote de oro. Su aroma fresco y chispeante, hecho de mandarina roja y menta es una invitación a la seducción. Luego viene el corazón, de una intensidad poco común que combina esencia de rosa y canela. Por último, el fondo reinterpreta la seducción en masculino gracias al acorde de cuero y ámbar. Al llegar, una multitud de fragancias que dejan una estela presente y con un sello propio.',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000373259768/2000373259768-1.jpg',
	 27990,
	 180,
	'perfumeria'),
(12,
	2000376844602,
	'LANCÔME',
	'COFRE LANCOME LA VIE EST BELLE EDP 30 ML + GEL DE DUCHA 50 ML + BODY LOTION 50 ML',
	'Eau de Parfum Vaporizador Haz tu vida incluso más hermosa con la nueva fragancia femenina de Lancôme, La Vie est Belle. Personificado por Julia Roberts, La Vie est Belle es una declaración universal de la belleza de la vida. Una marca olfativa esencia de perfume única creada por tres líderes perfumistas de Francia, La Vie est Belle presenta un nuevo lenguaje olfativo con preciosos ingredientes.',
	'https://home.ripley.cl/store/Attachment/WOP/D327/2000376844602/2000376844602-2.jpg',
	 29990,
	 100,
	'perfumeria');



