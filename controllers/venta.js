'use strict';
const db = require('../connection/connection');

function nuevaVenta(req, res) {
    let data = {v_id: req.body.v_id, v_prod_id: req.body.v_prod_id, v_cli_id: req.body.v_cli_id};
    let sql = "INSERT INTO ventas SET ? ";
    let query = db.query(sql, data, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
}


module.exports = {
    nuevaVenta
};
