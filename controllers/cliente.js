'use strict';
const db = require('../connection/connection');


const obtenerClientes = (req, res) => {

    let sql = `select * from clientes  `;
    db.query(sql, (error, result) => {
        if (error) throw error;
        res.json({
            ok: true,
            result
        });
    });
};


function nuevoCliente(req, res) {
    let data = {cli_nombre: req.body.cli_nombre, cli_apellido:req.body.cli_apellido,
                cli_rut: req.body.cli_rut,cli_telefono: req.body.cli_telefono,cli_email: req.body.cli_email,
        cli_nro_tarjeta: req.body.cli_nro_tarjeta,cli_direccion: req.body.cli_direccion};
    let sql = "INSERT INTO clientes SET ?  ";
    let query = db.query(sql, data,(err, results) => {
        if(err) throw err;
        return res.status(200).json({
            ok: true,
            results
        });
    });
}



module.exports = {
    obtenerClientes,
    nuevoCliente
};
