'use strict';
const db = require('../connection/connection');


const nuevaOrdenCompra = (req, res) => {

    let data = {od_v_id: req.body.od_v_id, oc_codigo: req.body.oc_codigo,oc_fecha_emision: req.body.oc_fecha_emision};
    let sql = "INSERT INTO orden_compra SET ? ";
    let query = db.query(sql, data, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
};

const obtenerOrdenCompra = (req, res) => {

    let oc_codigo = req.params.oc_codigo

    db.query(` select concat(c.cli_nombre,' ',c.cli_apellido) AS "nombre_cliente",
		  p.Prod_Precio "Precio_producto",
		c.cli_email AS "email_cliente",
		c.cli_direccion AS "direccion_cliente",
		c.cli_telefono AS "telefono_cliente",
		oc.oc_fecha_emision ,
		oc.oc_codigo AS "nro_orden_compra" from ventas v 
		JOIN productos p ON p.Prod_id = v.v_prod_id
		JOIN orden_compra oc ON   v.v_id = oc.od_id
		JOIN clientes c ON c.cli_id = v.v_cli_id
		WHERE oc.oc_codigo  =${oc_codigo}`, (error, result) => {
        if (error) throw error;
        res.json({
            ok: true,
            result
        });
    });

};


module.exports = {
    nuevaOrdenCompra,
    obtenerOrdenCompra
};
